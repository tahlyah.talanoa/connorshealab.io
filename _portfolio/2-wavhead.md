---
title: "WavHead"
image: "wavhead/wavhead-logo.svg"
thumbnail: "wavhead/wavhead-thumbnail.svg"
role: "Lead Front-End Developer"
description: "A relatively simple program to let people vote on music, a 'democratic DJ' if you will. It is hosted on the local WiFi network so only those on the same connection can vote on music. WavHead was built for playing music at parties."
link: "http://www.noided.media/WavHead"
date_range: "September — October 2014"
---
