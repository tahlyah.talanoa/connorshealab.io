---
title: "Fiction Dock"
image: "fictiondock/fictiondock-logo.png"
thumbnail: "fictiondock/fictiondock-thumbnail.svg"
role: "Lead Front-End Developer"
description: "Fiction Dock is a central repository for people to write and share their fan fiction. Fan fiction (per Wikipedia) is 'fiction about characters or settings from an original work of fiction, created by fans of that work rather than by its creator.'"
link: "http://www.fictiondock.com"
date_range: "June — August 2015"
---
