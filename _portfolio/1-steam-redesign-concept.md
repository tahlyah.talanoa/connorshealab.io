---
title: "Steam Redesign Concept"
image: "steam-redesign/steam-redesign-concept-logo.svg"
thumbnail: "steam-redesign/steam-redesign-concept-thumbnail.svg"
role: "Designer"
description: "A conceptual redesign of the Steam client. The redesign overhauls the current Steam client aesthetic with a simpler, more modern design. Note that this is not functional, and was only a concept mockup."
link: "https://www.behance.net/gallery/18931105/Steam-Redesign-Concept"
date_range: "July — August 2014"
---
